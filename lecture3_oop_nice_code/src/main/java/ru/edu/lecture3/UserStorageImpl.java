package ru.edu.lecture3;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class UserStorageImpl implements interface {@link UserStorage}
 */
public class UserStorageImpl implements UserStorage{

    private List<User> users;

    public UserStorageImpl(List<User> users) {
        this.users = users;
    }

    @Override
    public User getUserByLogin(String login) {

        if (login == null){
            throw new IllegalArgumentException("ERROR: login value is NULL!");
        }else if(login.equals(" ")){
            throw new IllegalArgumentException("ERROR: login value is blank!");
        }else if(login.isEmpty()){
            throw new IllegalArgumentException("ERROR: login value is empty!");
        }

        User requiredUser = null;
        Iterator<User> iter = users.iterator();

        while (iter.hasNext()){
            User nextUser = iter.next();
            if(nextUser.getLogin().equalsIgnoreCase(login)){
                requiredUser = nextUser;
                break;
            }
        }

        if (requiredUser == null){
            throw new RuntimeException("ERROR: user not found!");
        }

        return requiredUser;
    }

    @Override
    public User put(User user) {

        if (user.getLogin() == null || user.getLogin().equals(" ") || user.getLogin().isEmpty()
                || user.getFirstName() == null || user.getFirstName().equals(" ") || user.getFirstName().isEmpty()
                ||user.getLastName() == null || user.getLastName().equals(" ") || user.getLastName().isEmpty()
                || user.getBirthDate() == null || user.getGender() == null){

            throw new RuntimeException("ERROR: user has wrong data!");
        }

        users.add(user);

        return user;
    }

    @Override
    public User remove(String login) {

        User removedUser = getUserByLogin(login);
        users.remove(removedUser);

        return removedUser;
    }

    @Override
    public List<User> getAllUsers() {
        return users;
    }

    @Override
    public List<User> getAllUsersByGender(Gender gender){

        if (gender == null){
            throw new IllegalArgumentException("ERROR: gender value is NULL!");
        }

        List<User> result = new ArrayList<>();
        Iterator<User> iter = users.iterator();

        while (iter.hasNext()){
            User nextUser = iter.next();
            if(nextUser.getGender().equals(gender)){
                result.add(nextUser);

            }
        }

        return result;
    }

    @Override
    public int getUserAge(String login) {
        User requiredUser = getUserByLogin(login);

        LocalDate today = LocalDate.now();
        LocalDate birthday = requiredUser.getBirthDate();
        Period period = Period.between(birthday, today);
        return period.getYears();
    }
}
