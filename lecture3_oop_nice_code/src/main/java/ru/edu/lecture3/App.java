package ru.edu.lecture3;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) {

        List<User> users = new ArrayList<>();
        UserStorage userStorage = new UserStorageImpl(users);
        userStorage.put(new User("Vano", "Ivan", "Ivanov", Gender.MALE, LocalDate.of(1982, 12,21)));
        userStorage.put(new User("ALex", "Alex", "Aleksin", Gender.MALE, LocalDate.of(1981, 12,21)));
        userStorage.put(new User("Eva", "Eva", "Grin", Gender.FEMALE, LocalDate.of(1980, 12,21)));
        userStorage.put(new User("Anna23", "Anna", "Sidorova", Gender.FEMALE, LocalDate.of(1979, 12,21)));

    }
}
