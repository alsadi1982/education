import org.junit.Assert;
import org.junit.Test;
import ru.edu.lecture3.Gender;
import ru.edu.lecture3.User;
import ru.edu.lecture3.UserStorage;
import ru.edu.lecture3.UserStorageImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserStorageTests {

    private UserStorage userStorage = new UserStorageImpl(new ArrayList<>());

    /**
     * Checking the successful execution UserStorageImpl#put(User user)
     */
    @Test
    public void put_Success_Test() {

        User user1 = new User("Vano", "Ivan", "Ivanov", Gender.MALE, LocalDate.of(1982, 12,21));
        User user2 = new User("Igor", "Valera", "Titov", Gender.MALE, LocalDate.of(1972, 10,3));

        Assert.assertEquals(user1, userStorage.put(user1));
        Assert.assertEquals(user2, userStorage.put(user2));

    }

    /**
     * Checking the unsuccessful execution UserStorageImpl#put(User user)
        * Сheck the scenario in which UserStorageImpl#put(User user) takes a wrong data
     */
    @Test(expected = RuntimeException.class)
    public void put_Fail_Test() {

        User user1 = new User("Vano", "", "Ivanov", Gender.MALE, LocalDate.of(1982, 12,21));
        User user2 = new User("Igor", "Valera", "Titov", Gender.MALE, LocalDate.of(1972, 10,3));

        userStorage.put(user1);
        userStorage.put(user2);
//        try{
//            userStorage.put(user1);
////            Assert.fail("Expected RuntimeException");
//        }catch(RuntimeException ex){
//            Assert.assertEquals("ERROR: user has wrong data!",ex.getMessage());
//        }
//
//        try{
//            userStorage.put(user2);
//            Assert.fail("Expected RuntimeException");
//        }catch(RuntimeException ex){
//            Assert.assertEquals("ERROR: user has wrong data!",ex.getMessage());
//        }

    }

    /**
     * Checking the successful execution UserStorageImpl#getUserByLogin(String login)
     */
    @Test
    public void getUserByLogin_Success_Test(){

        User user1 = new User("Vano", "Ivan", "Ivanov", Gender.MALE, LocalDate.of(1982, 12,21));
        User user2 = new User("Igor", "Valera", "Titov", Gender.MALE, LocalDate.of(1972, 10,3));
        userStorage.put(user1);
        userStorage.put(user2);

        Assert.assertEquals(user1, userStorage.getUserByLogin("vano"));
        Assert.assertEquals(user2, userStorage.getUserByLogin("IGOR"));

    }

    /**
     * Checking the unsuccessful execution UserStorageImpl#getUserByLogin(String login)
        * Сheck the scenario in which UserStorageImpl#getUserByLogin(String login) takes login of user who is not in the storage
        * Сheck the scenario in which UserStorageImpl#getUserByLogin(String login)  login is empty
        * Сheck the scenario in which UserStorageImpl#getUserByLogin(String login) takes login is blank string
        * Сheck the scenario in which UserStorageImpl#getUserByLogin(String login) takes login is null
     */
    @Test
    public void getUserByLogin_Fail_Test(){

        User user1 = new User("Vano", "Ivan", "Ivanov", Gender.MALE, LocalDate.of(1982, 12,21));
        User user2 = new User("Igor", "Valera", "Titov", Gender.MALE, LocalDate.of(1972, 10,3));
        userStorage.put(user1);
        userStorage.put(user2);

        //takes login of user who is not in the storage
        try{
            userStorage.getUserByLogin("FAIL");
            Assert.fail("Expected RuntimeException");
        }catch(RuntimeException ex){
            Assert.assertEquals("ERROR: user not found!",ex.getMessage());
        }

        //login is empty
        try{
            userStorage.getUserByLogin("");
            Assert.fail("Expected IllegalArgumentException - ERROR: login value is empty!");
        }catch(RuntimeException ex){
            Assert.assertEquals("ERROR: login value is empty!",ex.getMessage());
        }

        //takes login is blank string
        try{
            userStorage.getUserByLogin(" ");
            Assert.fail("Expected IllegalArgumentException - ERROR: login value is blank!");
        }catch(RuntimeException ex){
            Assert.assertEquals("ERROR: login value is blank!",ex.getMessage());
        }

        //takes login is null
        try{
            userStorage.getUserByLogin(null);
            Assert.fail("Expected IllegalArgumentException - ERROR: login value is NULL!");
        }catch(RuntimeException ex){
            Assert.assertEquals("ERROR: login value is NULL!",ex.getMessage());
        }

    }

    /**
     * Checking the successful execution UserStorageImpl#remuve(String login)
     */
    @Test
    public void remove_Success_Test(){

        User user1 = new User("Vano", "Ivan", "Ivanov", Gender.MALE, LocalDate.of(1982, 12,21));
        User user2 = new User("Igor", "Valera", "Titov", Gender.MALE, LocalDate.of(1972, 10,3));
        userStorage.put(user1);
        userStorage.put(user2);

        Assert.assertEquals(user1, userStorage.remove("vano"));
        Assert.assertEquals(user2, userStorage.remove("IGOR"));
    }

    /**
     * Checking the unsuccessful execution UserStorageImpl#remove(String login)
     * Сheck the scenario in which UserStorageImpl#remove(String login) takes login of user who is not in the storage
     * Сheck the scenario in which UserStorageImpl#remove(String login)  login is empty
     * Сheck the scenario in which UserStorageImpl#remove(String login) takes login is blank string
     * Сheck the scenario in which UserStorageImpl#remove(String login) takes login is null
     */
    @Test
    public void remove_Fail_Test(){

        User user1 = new User("Vano", "Ivan", "Ivanov", Gender.MALE, LocalDate.of(1982, 12,21));
        User user2 = new User("Igor", "Valera", "Titov", Gender.MALE, LocalDate.of(1972, 10,3));
        userStorage.put(user1);
        userStorage.put(user2);

        //takes login of user who is not in the storage
        try{
            userStorage.remove("FAIL");
            Assert.fail("Expected RuntimeException");
        }catch(RuntimeException ex){
            Assert.assertEquals("ERROR: user not found!",ex.getMessage());
        }

        //login is empty
        try{
            userStorage.remove("");
            Assert.fail("Expected IllegalArgumentException - ERROR: login value is empty!");
        }catch(RuntimeException ex){
            Assert.assertEquals("ERROR: login value is empty!",ex.getMessage());
        }

        //takes login is blank string
        try{
            userStorage.remove(" ");
            Assert.fail("Expected IllegalArgumentException - ERROR: login value is blank!");
        }catch(RuntimeException ex){
            Assert.assertEquals("ERROR: login value is blank!",ex.getMessage());
        }

        //login is null
        try{
            userStorage.remove(null);
            Assert.fail("Expected IllegalArgumentException - ERROR: login value is NULL!");
        }catch(RuntimeException ex){
            Assert.assertEquals("ERROR: login value is NULL!",ex.getMessage());
        }

    }

    /**
     * Checking the successful execution UserStorageImpl#getUserAge(String login)
     */
    @Test
    public void getUserAge_Success_Test(){

        User user1 = new User("Vano", "Ivan", "Ivanov", Gender.MALE, LocalDate.of(1982, 12,21));
        User user2 = new User("Igor", "Valera", "Titov", Gender.MALE, LocalDate.of(1972, 10,3));
        userStorage.put(user1);
        userStorage.put(user2);

        Assert.assertEquals(39, userStorage.getUserAge("vano"));
        Assert.assertEquals(49, userStorage.getUserAge("IGOR"));
    }

    /**
     * Checking the unsuccessful execution UserStorageImpl#getUserAge(String login)
     * Сheck the scenario in which UserStorageImpl#getUserAge(String login) takes login of user who is not in the storage
     * Сheck the scenario in which UserStorageImpl#getUserAge(String login)  login is empty
     * Сheck the scenario in which UserStorageImpl#getUserAge(String login) takes login is blank string
     * Сheck the scenario in which UserStorageImpl#getUserAge(String login) takes login is null
     */
    @Test
    public void getUserAge_Fail_Test(){

        User user1 = new User("Vano", "Ivan", "Ivanov", Gender.MALE, LocalDate.of(1982, 12,21));
        User user2 = new User("Igor", "Valera", "Titov", Gender.MALE, LocalDate.of(1972, 10,3));
        userStorage.put(user1);
        userStorage.put(user2);

        //takes login of user who is not in the storage
        try{
            userStorage.getUserAge("FAIL");
            Assert.fail("Expected RuntimeException");
        }catch(RuntimeException ex){
            Assert.assertEquals("ERROR: user not found!",ex.getMessage());
        }

        //login is empty
        try{
            userStorage.getUserAge("");
            Assert.fail("Expected IllegalArgumentException - ERROR: login value is empty!");
        }catch(RuntimeException ex){
            Assert.assertEquals("ERROR: login value is empty!",ex.getMessage());
        }

        //takes login is blank string
        try{
            userStorage.getUserAge(" ");
            Assert.fail("Expected IllegalArgumentException - ERROR: login value is blank!");
        }catch(RuntimeException ex){
            Assert.assertEquals("ERROR: login value is blank!",ex.getMessage());
        }

        //takes login is null
        try{
            userStorage.getUserAge(null);
            Assert.fail("Expected IllegalArgumentException - ERROR: login value is NULL!");
        }catch(RuntimeException ex){
            Assert.assertEquals("ERROR: login value is NULL!",ex.getMessage());
        }

    }

    /**
     * Checking the successful execution UserStorageImpl#getAllUsersByGender(Gender gender)
     */
    @Test
    public void getAllUsersByGender_Success_Test() {

        User user1 = new User("Vano", "Ivan", "Ivanov", Gender.MALE, LocalDate.of(1982, 12, 21));
        User user2 = new User("Igor", "Valera", "Titov", Gender.MALE, LocalDate.of(1972, 10, 3));
        User user3 = new User("Anna23", "Anna", "Petrenko", Gender.FEMALE, LocalDate.of(1991, 9, 11));
        userStorage.put(user1);
        userStorage.put(user2);
        userStorage.put(user3);

        User[] expectedResult = {user1, user2};
        Object[] actualResult = userStorage.getAllUsersByGender(Gender.MALE).toArray();

        Assert.assertArrayEquals(expectedResult, actualResult);
    }

    /**
     * Checking the unsuccessful execution UserStorageImpl#getAllUsersByGender(Gender gender)
     * Сheck the scenario in which UserStorageImpl#getAllUsersByGender(Gender gender) takes gender is null
     */
    @Test
        public void getAllUsersByGender_Fail_Test(){

            User user1 = new User("Vano", "Ivan", "Ivanov", Gender.MALE, LocalDate.of(1982, 12,21));
            User user2 = new User("Igor", "Valera", "Titov", Gender.MALE, LocalDate.of(1972, 10,3));
            userStorage.put(user1);
            userStorage.put(user2);

            try{
                userStorage.getAllUsersByGender(null);
                Assert.fail("Expected IllegalArgumentException - ERROR: gender value is NULL!");
            }catch(IllegalArgumentException ex){
                Assert.assertEquals("ERROR: gender value is NULL!",ex.getMessage());
            }

        }

    /**
     * Checking the successful execution UserStorageImpl#getAllUsers()
     */
    @Test
    public void getAllUsers_Success_Test() {

        User user1 = new User("Vano", "Ivan", "Ivanov", Gender.MALE, LocalDate.of(1982, 12, 21));
        User user2 = new User("Igor", "Valera", "Titov", Gender.MALE, LocalDate.of(1972, 10, 3));
        User user3 = new User("Anna23", "Anna", "Petrenko", Gender.FEMALE, LocalDate.of(1991, 9, 11));
        userStorage.put(user1);
        userStorage.put(user2);
        userStorage.put(user3);

        User[] expectedResult = {user1, user2, user3};
        Object[] actualResult = userStorage.getAllUsers().toArray();

        Assert.assertArrayEquals(expectedResult, actualResult);

        userStorage.remove("vano");
        userStorage.remove("igor");
        userStorage.remove("anna23");

        User[] expectedResult2 = {};
        Object[] actualResult2 = userStorage.getAllUsers().toArray();

        Assert.assertArrayEquals(expectedResult2, actualResult2);

    }


}