package ru.edu.lecture3;

import java.io.*;
import java.util.*;

/**
 * Class FileAnalyserImpl implements interface {@link FileAnalyser}
 */
public class FileAnalyserImpl implements FileAnalyser{
    private File file;

    public FileAnalyserImpl(String filePath) {
        this.file = new File(filePath);
    }

    @Override
    public String getFileName() {
        String fileName = file.getName();
        return fileName;
    }

    @Override
    public int getRowsCount() {
        int rowsCount = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(this.file))){
            String line = reader.readLine();
            while (line != null){
                rowsCount++;
                line = reader.readLine();
            }

        }catch (FileNotFoundException ex){
            System.out.println("ERROR: " + ex.getMessage());
            throw new IllegalArgumentException("UNKNOWN ERROR!");
//            ex.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("new UNKNOWN ERROR!");
        }
        return rowsCount;
    }

    @Override
    public int getLettersCount() {

        int lettersCount = 0;

        try (BufferedReader reader = new BufferedReader(new FileReader(this.file))){
            String line = reader.readLine();
            while (line != null){
                lettersCount += line.replaceAll("\\s","").length();
                line = reader.readLine();
            }
        }catch (FileNotFoundException ex){
            System.out.println("ERROR: " + ex.getMessage());
            throw new IllegalArgumentException("UNKNOWN ERROR!");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return lettersCount;
    }

    @Override
    public Map<Character, Integer> getSymbolsStatistics() {

        Map<Character, Integer> map = createMap();
        return map;
    }

    @Override
    public List<Character> getTopNPopularSymbols(int n) {

        List<Character> list = new ArrayList<>();
        Map<Character, Integer> map = createMap();
        int mapSize = map.size();

        for (int i = 0; i < n && i < mapSize; i++){
            int max = getMax(map);
            Character maxChar = null;
            for (Map.Entry<Character, Integer> elem: map.entrySet()){
                if (max == elem.getValue()){
                    maxChar = elem.getKey();
                    break;
                }
            }
            list.add(maxChar);
            map.remove(maxChar);
        }


        return list;
    }

    @Override
    public void saveSummary(String filePath) {

        String summary = "Filename: " + getFileName() + "\n" + "rowscount: " + getRowsCount() + "\n" + "totalSymbols: " + getLettersCount() + "\n" + "symbolsStatistics: " + getSymbolsStatistics() + "\n" + "top3PopularSymbols: " + getTopNPopularSymbols(3) + "\n";

        try(FileWriter writer = new FileWriter(filePath)){
            writer.write(summary);

        }catch (IOException ex){
            throw new RuntimeException("Failed to save!");
        }

    }

    /**
     * method getMax() return maximum value of map elements
     */
    private int getMax(Map<Character, Integer> map){
        int max = 0;
        for(Map.Entry<Character, Integer> elem: map.entrySet()){
            if (max < elem.getValue()){
                max = elem.getValue();
            }
        }

        return max;
    }

    /**
     * method createMap() creates map with symbols statistic and return it
     */
    private Map<Character, Integer> createMap(){

        Map<Character, Integer> map = new TreeMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(this.file))){
            String line = reader.readLine();

            while (line != null){
                char[] chars = line.replaceAll("\\s","").toCharArray();
                for (char elem: chars){
                    if (Character.isLetterOrDigit(elem)){
                        if (map.containsKey(elem)){
                            int value = map.get(elem);
                            value++;
                            map.put(elem, value);
                        }else{
                            map.put(elem, 1);
                        }
                    }
                }
                line = reader.readLine();
            }
        }catch (FileNotFoundException ex){
            System.out.println("ERROR: " + ex.getMessage());
            ex.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;
    }
}
