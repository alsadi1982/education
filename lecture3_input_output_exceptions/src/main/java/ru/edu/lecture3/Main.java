package ru.edu.lecture3;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) {
        FileAnalyser fileAnalyser = new FileAnalyserImpl("inp.txt");
        System.out.println(fileAnalyser.getFileName());
        try {
            System.out.println(fileAnalyser.getRowsCount());
        }catch (IllegalArgumentException ex){
            System.out.println(ex.getMessage());
        }

        try {
            System.out.println(fileAnalyser.getLettersCount());
        }catch (IllegalArgumentException ex){
            System.out.println(ex.getMessage());
        }
//        System.out.println(fileAnalyser.getSymbolsStatistics());
//        System.out.println(fileAnalyser.getTopNPopularSymbols(7));
//        fileAnalyser.saveSummary("summary.txt");

    }
}
