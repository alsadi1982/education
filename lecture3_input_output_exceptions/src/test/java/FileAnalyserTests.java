import org.junit.Assert;
import org.junit.Test;
import ru.edu.lecture3.FileAnalyser;
import ru.edu.lecture3.FileAnalyserImpl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class FileAnalyserTests {

    private FileAnalyserImpl fileAnalyser = null;

    /**
     * Checking the successful execution FileAnalyserImpl#getFileName()
     */
    @Test
    public void getFileName_Success_Test() throws IOException {
        String filePath = File.createTempFile("input",".txt").getName();
        fileAnalyser = new FileAnalyserImpl(filePath);
        Assert.assertEquals(filePath, fileAnalyser.getFileName());
        Assert.assertNotEquals("enter.txt", fileAnalyser.getFileName());
    }

    /**
     * Checking the successful execution FileAnalyserImpl#getRowsCount()
     */
    @Test
    public void getRowsCount_Success_Test() throws IOException{
        String data1 = "Hello" + "\n" + "world";
        String data2 = "Hello";
        String filePath = File.createTempFile("input",".txt").getPath();
        FileWriter writer = new FileWriter(filePath);
        writer.write(data1);
        writer.close();
        fileAnalyser = new FileAnalyserImpl(filePath);

        Assert.assertEquals(2, fileAnalyser.getRowsCount());
        FileWriter writer2 = new FileWriter(filePath);
        writer2.write(data2);
        writer2.close();
        Assert.assertEquals(1, fileAnalyser.getRowsCount());


    }

    /**
     * Checking the successful execution FileAnalyserImpl#getLettersCount()
     */
    @Test
    public void getLettersCount_Success_Test() throws IOException{
        String data1 = "Hello" + "\n" + " world!";
        String filePath = File.createTempFile("input",".txt").getPath();
        FileWriter writer = new FileWriter(filePath);
        writer.write(data1);
        writer.close();
        fileAnalyser = new FileAnalyserImpl(filePath);

        Assert.assertEquals(11, fileAnalyser.getLettersCount());
        FileWriter writer2 = new FileWriter(filePath);
    }

    /**
     * Checking the successful execution FileAnalyserImpl#getTopNPopularSymbols(int n)
     */
    @Test
    public void getTopNPopularSymbols_Success_Test() throws IOException{
        String data1 = "ddddoopwecs";
        String filePath = File.createTempFile("input",".txt").getPath();
        FileWriter writer = new FileWriter(filePath);
        writer.write(data1);
        writer.close();
        fileAnalyser = new FileAnalyserImpl(filePath);

        Character[] expectedResult = {'d', 'o', 'c', 'e', 'p', 's', 'w'};

        Assert.assertArrayEquals(expectedResult, fileAnalyser.getTopNPopularSymbols(7).toArray());
        FileWriter writer2 = new FileWriter(filePath);
    }

    /**
     * Checking the successful execution FileAnalyserImpl#getSymbolsStatistics()
     */
    @Test
    public void getSymbolsStatistics_Success_Test() throws IOException{
        String data1 = "Hello" + "\n" + " world!";
        String filePath = File.createTempFile("input",".txt").getPath();
        FileWriter writer = new FileWriter(filePath);
        writer.write(data1);
        writer.close();
        fileAnalyser = new FileAnalyserImpl(filePath);

        Map<Character, Integer> expectedMap= new TreeMap<>();
        expectedMap.put('H', 1);
        expectedMap.put('e', 1);
        expectedMap.put('l', 3);
        expectedMap.put('o', 2);
        expectedMap.put('w', 1);
        expectedMap.put('r', 1);
        expectedMap.put('d', 1);


        Assert.assertEquals(expectedMap.toString(), fileAnalyser.getSymbolsStatistics().toString());
    }


}